#-------------------------------------------------
#
# Project created by QtCreator 2015-02-09T11:04:54
#
#-------------------------------------------------

QT      += core
QT      += gui
QT      += winextras

TARGET = PrintScreenCatcher
CONFIG   += console static

CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
