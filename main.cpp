#include <QGuiApplication>
#include <QPixmap>
#include <QtWin>
#include <QDateTime>
#include <QAbstractNativeEventFilter>

#include <Windows.h>
#include <iostream>
#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>


class MyApplication : public QGuiApplication
{
public:

    class HotKeyEventHanlder : public QAbstractNativeEventFilter
    {
    public:
        QString m_saveTo;
        HotKeyEventHanlder(const QString & saveTo) : m_saveTo(saveTo) {}

        virtual bool nativeEventFilter(const QByteArray & , void * message, long * /*result*/) override
        {
            MSG* msg = reinterpret_cast<MSG*>(message);
            if (msg->message == WM_HOTKEY)
            {
                if (!OpenClipboard(NULL)) {
                    _tprintf(_T("Can't open clipboard\n"));
                    return false;
                }
                HANDLE hBitmapClipboard = GetClipboardData(CF_BITMAP);
                if (!hBitmapClipboard) {
                    _tprintf(_T("Not Bitmap in clipboard\n"));
                    return false;
                }

                QPixmap pixmap = QtWin::fromHBITMAP((HBITMAP)hBitmapClipboard);
                QDateTime now = QDateTime::currentDateTime();
                QString filename = QString("%1/%2.png")
                        .arg(m_saveTo)
                        .arg(now.toString("yyyy-MM-dd hh_mm_ss"));
                pixmap.save(filename, "PNG");
                _tprintf(_T("Screenshot was saved to %s\n"), qPrintable(filename));
                CloseClipboard();
                return true;
            }
            else if(msg->message == WM_KEYDOWN) {
                 //&& msg->lParam == VK_ESCAPE
                quit();
            }
            else if(msg->message == WM_KEYFIRST) {
                 //&& msg->lParam == VK_ESCAPE
                quit();
            }
            return false;
        }
    };

    MyApplication(int &argc, char **argv) : QGuiApplication(argc, argv)
    {
        if (!RegisterHotKey(NULL, 1, MOD_CONTROL | MOD_ALT, VK_SNAPSHOT)) {
            LPVOID lpMsgBuf;
            LPVOID lpDisplayBuf;
            DWORD dw = GetLastError();

            FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER |
                FORMAT_MESSAGE_FROM_SYSTEM |
                FORMAT_MESSAGE_IGNORE_INSERTS,
                NULL,
                dw,
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                (LPTSTR) &lpMsgBuf,
                0, NULL );

            // Display the error message and exit the process

            lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
                (lstrlen((LPCTSTR)lpMsgBuf) + 40) * sizeof(TCHAR));
            StringCchPrintf((LPTSTR)lpDisplayBuf,
                LocalSize(lpDisplayBuf) / sizeof(TCHAR),
                TEXT("Failed with error %d: %s"),
                dw, lpMsgBuf);
            LocalFree(lpMsgBuf);
            LocalFree(lpDisplayBuf);
            throw std::runtime_error((const char*)lpDisplayBuf);
        }

        m_hotKeyHanlder = new HotKeyEventHanlder(arguments().value(1, applicationDirPath()));
        _tprintf(_T("Hotkey 'CTRL+ALT-PrnScr' registered\nScreenshot will save to: %s\n"), qPrintable(m_hotKeyHanlder->m_saveTo));

        installNativeEventFilter(m_hotKeyHanlder);
    }

    ~MyApplication()
    {
        UnregisterHotKey(NULL, 1);
    }

    HotKeyEventHanlder * m_hotKeyHanlder;
};

int main(int argc, char *argv[])
{
    MyApplication a(argc, argv);

    return a.exec();
}
